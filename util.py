from models.reasoning import ReasonerPackage, ReasonerInput, ReasonerParams
from models.repository import Interpretation
from typing import Tuple, List
import os
import time


def write_dependencies(interpretations: List[Interpretation], reasoner_folder: str):
    for interpretation in interpretations:
        if interpretation.dependencies is not None:
            for dependency in interpretation.dependencies:
                dependency_file = os.path.join(
                    reasoner_folder, dependency.title)
                with open(dependency_file, "w") as f:
                    f.write(dependency.content)


def write_file(path: str, content: str):
    with open(path, "w") as f:
        f.write(content)


def init_reasoner_folder(reasoner_package: ReasonerPackage, reasoner_input: ReasonerInput,
                         reasoner_params: ReasonerParams, language_suffix: str, reasoner_folder: str = None) -> Tuple[str, str]:
    # Create folder that is the starting point for reasoner
    # We do not remove this folder, as we don't know when the reasoner is done
    if reasoner_folder is None:
        reasoner_folder = f"temp-{language_suffix}-{time.time_ns()}"
        while os.path.exists(reasoner_folder):
            reasoner_folder = f"temp-{language_suffix}-{time.time_ns()}"

    if not os.path.exists(reasoner_folder):
        os.makedirs(reasoner_folder)

    write_dependencies(reasoner_input.interpretations, reasoner_folder)
    reasoner_file_path = os.path.join(
        reasoner_folder, f"reasoner.{language_suffix}")

    params_addition = reasoner_params.content if reasoner_params is not None else ""
    joint = reasoner_package.joint + \
        ("\n") + params_addition
    write_file(reasoner_file_path, joint)

    return (reasoner_file_path, joint)


def eflint_code_combinator(eflint_code: str):
    # remove duplicate #require statements
    lines = eflint_code.split("\n")
    unique_lines = []
    seen = set()
    for line in lines:
        if line.strip().startswith("#require"):
            if line not in seen:
                seen.add(line)
                unique_lines.append(line)
        else:
            unique_lines.append(line)
    return "\n".join(unique_lines)
