import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

export default ({ mode }) => {
  const base = mode === "gitlab_deploy" ? "/prototype-normative-sources-framework/" : "/";
  return defineConfig({
    plugins: [react()],
    base: base,
  });
};
