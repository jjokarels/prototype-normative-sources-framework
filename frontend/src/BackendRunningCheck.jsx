import { useEffect, useState } from "react";
import API_URL from "./API_URL";

export default function BackendRunningCheck({ children }) {
  const [backendRunning, setBackendRunning] = useState(false);

  useEffect(() => {
    const checkBackendRunning = async () => {
      const response = await fetch(`${API_URL}/normative_sources`);
      const data = await response.json();
      if (Array.isArray(data)) {
        setBackendRunning(true);
      }
    };
    checkBackendRunning();
  }, []);

  if (backendRunning) return children;

  const handleRefresh = () => window.location.reload();

  return (
    <div className="container text-center py-5">
      <h1 className="fw-bold">Back end not running</h1>
      <span>
        The required back end does not appear to be running on <b>localhost:7979</b>.
        <br />
        If the back end is running on a different address, please change the <b>API_URL</b> in{" "}
        <b>frontend\src\API_URL.js</b>.<br /> You may need to run this React application locally.
      </span>
      <br />

      <button className="btn btn-primary mt-2" onClick={handleRefresh}>
        Refresh
      </button>
    </div>
  );
}
