import { useContext, useState } from "react";
import { AppContext } from "../../App";
import ReactDiffViewer, { DiffMethod } from "react-diff-viewer-continued";

export default function DiffViewer() {
  const { interpretations } = useContext(AppContext);
  const [interpretation1, setInterpretation1] = useState(interpretations[0] ?? null);
  const [interpretation2, setInterpretation2] = useState(interpretations[1] ?? null);

  const handleChange = (e, setter) => {
    setter(interpretations.find((i) => i.id === e.target.value));
  };

  return (
    <div className="card">
      <div className="card-header">
        <h3 className="card-title">Diff Viewer for Interpretations</h3>
        <small>This component uses react-diff-viewer-continued</small>
        <div className="row">
          {[interpretation1, interpretation2].map((interpretation, index) => (
            <div className="col-md-6" key={index}>
              <div className="form-group">
                <label>
                  {index === 0 ? "Selected Interpretation" : "Interpretation to Compare"}
                </label>
                <select
                  className="form-control"
                  onChange={(e) =>
                    handleChange(e, index === 0 ? setInterpretation1 : setInterpretation2)
                  }
                  value={interpretation?.id ?? "null"}
                >
                  <option value="null" disabled>
                    Select an interpretation
                  </option>
                  {interpretations.map((i) => (
                    <option
                      key={i.id}
                      value={i.id}
                      disabled={i.id === (index === 0 ? interpretation2.id : interpretation1.id)}
                    >
                      Interpretation {i.id} ({i.language})
                    </option>
                  ))}
                </select>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="card-body">
        {interpretation1 && interpretation2 && (
          <ReactDiffViewer
            oldValue={interpretation1.content}
            newValue={interpretation2.content}
            splitView={true}
            compareMethod={DiffMethod.WORDS}
          />
        )}
      </div>
    </div>
  );
}
