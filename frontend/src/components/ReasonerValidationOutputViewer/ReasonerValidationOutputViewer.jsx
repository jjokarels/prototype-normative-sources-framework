import { useContext, useEffect, useMemo, useState } from "react";
import API_URL from "../../API_URL";
import { AppContext } from "../../App";

export default function ReasonerValidationOutputViewer({ reasonerValidationOutput }) {
  const { output, is_valid, id } = reasonerValidationOutput;
  const { fetchReasonerOutputs, reasoners, setActiveItem } = useContext(AppContext);

  const input = useMemo(() => output.reasoner_input, [output]);
  const language = useMemo(() => input.language, [input]);
  const interpretations = useMemo(() => input.interpretations, [input]);

  const relevantReasoners = reasoners
    .map((reasoner, index) => {
      return { reasoner, index };
    })
    .filter(({ reasoner }) => reasoner.language === language);

  const [reasonerStarted, setReasonerStarted] = useState(false);
  const [availableParams, setAvailableParams] = useState(null);
  const [selectedParam, setSelectedParam] = useState(null);
  const [selectedReasoner, setSelectedReasoner] = useState(
    relevantReasoners.length > 0 ? relevantReasoners[0].index : -1
  );

  const startReasoner = async () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
    };

    try {
      const response = await fetch(
        API_URL +
          "/launch_reasoner_validation_output/" +
          id +
          "?reasoner_index=" +
          selectedReasoner +
          (selectedParam ? "&reasoner_params_id=" + selectedParam.id : ""),
        requestOptions
      );
      if (!response.ok) throw new Error("Request failed with status: " + response.status);
      const data = await response.json();
      alert("Reasoner started: " + JSON.stringify(data?.output));
      fetchReasonerOutputs();
    } catch (error) {
      alert("Error starting reasoner: " + error);
    }

    setReasonerStarted(true);
  };

  useEffect(() => {
    if (availableParams === null) {
      fetch(API_URL + "/reasoner_params")
        .then((response) => response.json())
        .then((data) => {
          setAvailableParams(data);
        })
        .catch((error) => alert("Error fetching reasoner params: " + error));
    }
  }, [availableParams]);

  useEffect(() => {
    setReasonerStarted(false);
  }, [reasonerValidationOutput]);

  useEffect(() => {
    setSelectedParam(null);
  }, [reasonerStarted]);

  const paramsForLanguage = useMemo(
    () => availableParams?.filter((param) => param.language === language),
    [availableParams, language]
  );

  return (
    <div className="card">
      <div className="card-header">
        <h4>Reasoner Validation Output ID: {id}</h4>
        <h5>
          <b>Validity</b>: {is_valid ? "Valid" : "Invalid"}
          {" - "}
          <b>Language</b>: {language}
        </h5>
      </div>
      <div className="card-body">
        <h5>Interpretations:</h5>
        <ul className="list-group">
          {interpretations.map((interpretation) => (
            <li
              key={interpretation.id}
              className="list-group-item list-group-item-action cursor-pointer"
              onClick={() => setActiveItem({ type: "interpretation", id: interpretation.id })}
            >
              <span>
                <b>Interpretation #{interpretation.id}</b> (interprets fragment #
                {interpretation.fragment_id})
              </span>
            </li>
          ))}
        </ul>
        {is_valid ? (
          <>
            <div>
              <h5>Joint specification:</h5>
              <div className="border rounded p-3 mb-2">
                <pre>{output.joint}</pre>
              </div>

              {!reasonerStarted && (
                <>
                  <button
                    className="btn btn-primary"
                    onClick={startReasoner}
                    disabled={selectedReasoner === -1}
                  >
                    Start{" "}
                    {selectedParam
                      ? `with parameter set #${selectedParam.id}`
                      : "without parameters"}
                  </button>
                  {paramsForLanguage && (
                    <div className="mt-2">
                      <h5>Parameters:</h5>
                      <select
                        className="form-select"
                        onChange={(e) => setSelectedParam(paramsForLanguage[e.target.value])}
                        defaultValue={null}
                      >
                        <option value={null}>Start With No Parameters</option>
                        {paramsForLanguage.map((param, index) => (
                          <option key={index} value={index}>
                            {param.id}: {param.content}
                          </option>
                        ))}
                      </select>
                    </div>
                  )}
                  {relevantReasoners.length > 0 && (
                    <div className="mt-2">
                      <h5>Reasoner:</h5>
                      <select
                        className="form-select"
                        onChange={(e) => setSelectedReasoner(parseInt(e.target.value))}
                        value={selectedReasoner}
                      >
                        {relevantReasoners.map(({ reasoner, index }) => (
                          <option key={index} value={index}>
                            {reasoner.name} (Does {reasoner.use_params && "not"} use params) (Does{" "}
                            {!reasoner.has_reasoner_output && "not"} have relevant reasoner output)
                          </option>
                        ))}
                      </select>
                    </div>
                  )}
                </>
              )}
              {reasonerStarted && (
                <>
                  <button className="btn btn-success" disabled>
                    Started
                  </button>
                  <button
                    className="btn btn-outline-primary ms-2"
                    onClick={() => setReasonerStarted(false)}
                  >
                    Reset
                  </button>
                </>
              )}
            </div>
          </>
        ) : (
          <div className="card-header border-top">
            {Object.keys(output.error).map((key) => (
              <div key={key}>
                <b>{key}</b>: {output.error[key]}
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
}
