import { useState } from "react";
import { useSearch } from "../../hooks/useSearch";

export default function SearchComponent({ data, setActiveItem }) {
  const [query, setQuery] = useState("");
  const searchResults = useSearch(data, query).slice(0, 10);
  const isNormativeSource = (result) =>
    result.item.id.startsWith("normative-source") &&
    !result.item.id.startsWith("normative-source-version") &&
    !result.item.id.startsWith("normative-source-fragment");
  const isInterpretation = (result) =>
    result.item.id.startsWith("interpretation") &&
    !result.item.id.startsWith("interpretation-dependency");

  const handleQueryChange = (event) => setQuery(event.target.value);

  return (
    <div>
      <h5>Search</h5>
      <input type="text" className="form-control" value={query} onChange={handleQueryChange} />
      {searchResults.some((result) => result.score > 0.0) ? (
        <ul className="list-group mt-2">
          {searchResults.map((result) => (
            <li key={result.item.id} className="list-group-item">
              <b>{result.item.id}</b> <br />
              {`"${result.scoreValue}"`}
              <div className="d-flex justify-content-between">
                <small>{parseFloat(result.score).toFixed(4)}</small>
                {isNormativeSource(result) && (
                  <button
                    className="btn btn-sm btn-primary"
                    onClick={() =>
                      setActiveItem({ type: "normativeSource", id: result.item.id.split("-")[2] })
                    }
                  >
                    View
                  </button>
                )}
                {isInterpretation(result) && (
                  <button
                    className="btn btn-sm btn-primary"
                    onClick={() =>
                      setActiveItem({ type: "interpretation", id: result.item.id.split("-")[1] })
                    }
                  >
                    View
                  </button>
                )}
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <p className="mt-2">{query ? "No results found" : "Type to search"}</p>
      )}
    </div>
  );
}
