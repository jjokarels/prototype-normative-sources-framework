import { useContext, useState } from "react";
import { AppContext } from "../../App";

export default function ReasonerOutputViewer({ reasonerOutput }) {
  const { setActiveItem } = useContext(AppContext);
  const [showJson, setShowJson] = useState(false);
  const [showParams, setShowParams] = useState(false);

  return (
    <div className="card">
      <div className="card-header">
        <h4>Reasoner Output #{reasonerOutput.id}</h4>
        <h6>
          <button
            className="btn btn-primary btn-sm"
            onClick={() =>
              setActiveItem({
                type: "reasonerValidationOutput",
                id: reasonerOutput.reasoner_validation_output_id,
              })
            }
          >
            Input: Reasoner Validation Output #{reasonerOutput.reasoner_validation_output_id}
          </button>
        </h6>
      </div>
      <div className="card-body">
        <h5>Output</h5>
        <pre>{reasonerOutput.output}</pre>
      </div>
      <div className="card-footer">
        <div className="d-flex gap-2 mb-2">
          <button className="btn btn-primary" onClick={() => setShowJson(!showJson)}>
            {showJson ? "Hide" : "Show"} Output as JSON
          </button>
          <button className="btn btn-primary" onClick={() => setShowParams(!showParams)}>
            {showParams ? "Hide" : "Show"} Parameters
          </button>
        </div>
        {showJson && (
          <div>
            <h5>JSON</h5>
            <pre>{JSON.stringify(reasonerOutput)}</pre>
          </div>
        )}
        {showParams && (
          <div>
            <h5>Parameters</h5>
            <pre>{JSON.stringify(reasonerOutput.reasoner_params)}</pre>
          </div>
        )}
      </div>
    </div>
  );
}
