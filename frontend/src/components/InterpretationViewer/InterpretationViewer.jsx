export default function InterpretationViewer({ interpretation }) {
  const { content, id, language, dependencies } = interpretation;

  return (
    <div className="card">
      <div className="card-header">
        <h4>Interpretation #{id}</h4> <b>language: {language}</b>
      </div>
      <div className="card-body">
        <pre>{content}</pre>
      </div>
      {dependencies && (
        <div className="card-footer">
          <h5>Dependencies</h5>
          <ul>
            {dependencies.map((dependency) => (
              <li key={dependency.id}>
                <b>{dependency.title}</b>
                <pre>{dependency.content}</pre>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
