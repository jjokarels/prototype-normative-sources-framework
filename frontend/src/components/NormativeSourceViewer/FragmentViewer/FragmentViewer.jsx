import { useContext } from "react";
import { AppContext } from "../../../App";
import { SelectionContext } from "../NormativeSourceViewer";

function InterpretationList({ fragmentId, index }) {
  const { selectedInterpretations, setSelectedInterpretations } = useContext(SelectionContext);
  const { setActiveItem, interpretations } = useContext(AppContext);

  const myInterpretations = interpretations.filter(
    (interpretation) => interpretation.fragment_id === fragmentId
  );

  const handleInterpretationChange = (interpretationId) => {
    setSelectedInterpretations((prev) => [
      ...prev.slice(0, index),
      interpretationId,
      ...prev.slice(index + 1),
    ]);
  };

  return (
    <div className="rounded border text-center p-2 h-100">
      <small>
        <b>Interpretations:</b>
      </small>
      <div className="col-12">
        {myInterpretations.map((interpretation) => (
          <div className="form-check" key={interpretation.id}>
            <input
              className="form-check-input"
              type="checkbox"
              checked={selectedInterpretations[index] === interpretation.id}
              onChange={() => handleInterpretationChange(interpretation.id)}
            />
            <button
              type="button"
              className="btn btn-outline-primary btn-sm w-100 text-break"
              onClick={() => setActiveItem({ type: "interpretation", id: interpretation.id })}
            >
              Interpretation #{interpretation.id}{" "}
              <div className="float-end fw-bold">{interpretation.language}</div>
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}

export default function FragmentViewer({ fragments }) {
  const { interpretations } = useContext(AppContext);

  return fragments.map((fragment, index) => {
    const hasInterpretations = interpretations.some(
      ({ fragment_id }) => fragment_id === fragment.id
    );

    return (
      <div key={fragment.id} className="row py-2 border-bottom">
        <div className="col-8">
          <b>Fragment #{fragment.id}</b>
          <p>{fragment.content}</p>
        </div>
        {hasInterpretations && (
          <div className="col-4">
            <InterpretationList fragmentId={fragment.id} index={index} />
          </div>
        )}
      </div>
    );
  });
}
