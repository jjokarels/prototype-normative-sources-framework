import { createContext, useContext, useEffect, useState } from "react";
import ValidateSelector from "./ValidateSelector/ValidateSelector";
import FragmentViewer from "./FragmentViewer/FragmentViewer";
import { AppContext } from "../../App";

export const SelectionContext = createContext();

const NormativeSourceViewer = ({ normativeSource }) => {
  const [selectedVersion, setSelectedVersion] = useState(null);
  const [selectedInterpretations, setSelectedInterpretations] = useState([]);
  const { interpretations } = useContext(AppContext);

  useEffect(() => {
    setSelectedVersion(normativeSource.versions[0] || null);
  }, [normativeSource]);

  useEffect(() => {
    setSelectedInterpretations(
      (selectedVersion?.fragments || []).map((fragment) => {
        const firstInterpretation = interpretations.find(
          (interpretation) => interpretation.fragment_id === fragment?.id
        );
        return firstInterpretation?.id ?? null;
      })
    );
  }, [selectedVersion, interpretations]);

  const handleVersionChange = (version) => {
    if (version === selectedVersion) return;
    setSelectedVersion(version);
  };

  const languages = selectedInterpretations.map((interpretation) => {
    const selectedInterpretation = interpretations.find((i) => i.id === interpretation);
    return selectedInterpretation?.language ?? null;
  });

  if (!selectedVersion) return null;

  return (
    <SelectionContext.Provider
      value={{ selectedInterpretations, setSelectedInterpretations, languages }}
    >
      <div className="card">
        <div className="card-header">
          <h4>
            {normativeSource.title} (Normative Source #{normativeSource.id})
          </h4>
          <div className="btn-group">
            {normativeSource.versions.map((version) => (
              <button
                key={version.version}
                type="button"
                className={`btn btn-${selectedVersion === version ? "primary" : "secondary"}`}
                onClick={() => handleVersionChange(version)}
              >
                {version.version}
              </button>
            ))}
          </div>
        </div>
        <div className="card-body">
          <FragmentViewer fragments={selectedVersion.fragments} />
        </div>
        {selectedInterpretations.some((interpretation) => interpretation !== null) && (
          <div className="card-footer d-flex gap-2 align-items-center">
            <span>
              Selected Interpretations:{" "}
              {selectedInterpretations.filter((i) => i !== null).length > 0 && "#"}
              {selectedInterpretations.filter((i) => i !== null).join(", #")}
            </span>
            <ValidateSelector interpretationSelection={selectedInterpretations} />
          </div>
        )}
      </div>
    </SelectionContext.Provider>
  );
};

export default NormativeSourceViewer;
