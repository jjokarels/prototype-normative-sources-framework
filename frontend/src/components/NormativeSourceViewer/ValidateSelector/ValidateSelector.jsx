import { useContext, useEffect, useState } from "react";
import API_URL from "../../../API_URL";
import { AppContext } from "../../../App";
import { SelectionContext } from "../NormativeSourceViewer";

export default function ValidateSelector() {
  const { fetchReasonerValidationOutputs, setActiveItem, validators } = useContext(AppContext);
  const { selectedInterpretations, languages } = useContext(SelectionContext);

  const [response, setResponse] = useState(null);
  const [loading, setLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  const findSelectedValidatorIndex = (validators, languages) =>
    validators && languages
      ? validators
          .map((validator, index) => ({ validator, index }))
          .find(({ validator }) => languages.includes(validator.language)).index
      : 0;

  const [selectedValidator, setSelectedValidator] = useState(
    findSelectedValidatorIndex(validators, languages)
  );

  useEffect(() => {
    setResponse(null);
    setValidated(false);
  }, [selectedInterpretations]);

  useEffect(() => {
    setSelectedValidator(findSelectedValidatorIndex(validators, languages));
  }, [validators, languages]);

  const handleValidate = async () => {
    setLoading(true);
    try {
      const response = await fetch(
        API_URL + "/validation_for_reasoning?validator_index=" + selectedValidator,
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(selectedInterpretations.filter((i) => i !== null)),
        }
      );
      const json = await response.json();
      setResponse(json);
      await fetchReasonerValidationOutputs();
      if (json.is_valid) setValidated(true);
      if (!json.is_valid) throw new Error("Invalid interpretation code");
    } catch (error) {
      alert("Validation error: \n" + JSON.stringify(error));
      setResponse(null);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="d-flex flex-row gap-2">
      {validated && (
        <button
          className="btn btn-success"
          onClick={() => setActiveItem({ type: "reasonerValidationOutput", id: response?.id })}
        >
          Validated, see Reasoner Validation Output #{response?.id}
        </button>
      )}
      {!validated && (
        <>
          <button
            className="btn btn-primary"
            onClick={handleValidate}
            disabled={
              loading || validated || !selectedInterpretations.length || selectedValidator === null
            }
          >
            {loading ? "Validating..." : "Validate"}
          </button>
          <select
            className="form-select"
            value={selectedValidator}
            onChange={(e) => setSelectedValidator(parseInt(e.target.value))}
          >
            {validators?.map((validator, index) => (
              <option
                key={JSON.stringify(validator)}
                value={index}
                disabled={!languages.includes(validator.language)}
              >
                {validator.name} (Language: {validator.language})
              </option>
            ))}
          </select>
        </>
      )}
    </div>
  );
}
