export const createSearchIndex = (data) => {
  const [normativeSources, interpretations] = data;
  const myData = [];

  normativeSources.forEach((item) => {
    myData.push({ id: `normative-source-${item.id}`, content: item.title });
    item.versions?.forEach((version) => {
      myData.push({ id: `normative-source-version-${version.id}`, content: version.version });
      version.fragments.forEach((fragment) => {
        myData.push({ id: `normative-source-fragment-${fragment.id}`, content: fragment.content });
      });
    });
  });

  interpretations.forEach((item) => {
    myData.push({ id: `interpretation-${item.id}`, content: item.content });
    item.dependencies?.forEach((dependency) => {
      myData.push({ id: `interpretation-dependency-${dependency.id}`, content: dependency.title });
    });
  });

  return Array.from(new Map(myData.map((item) => [item.id, item])).values());
};
