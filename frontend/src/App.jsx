import { createContext, useState } from "react";
import API_URL from "./API_URL";
import DiffViewer from "./components/DiffViewer/DiffViewer";
import SearchComponent from "./components/SearchComponent/SearchComponent";
import ReasonerOutputViewer from "./components/ReasonerOutputViewer/ReasonerOutputViewer";
import NormativeSourceViewer from "./components/NormativeSourceViewer/NormativeSourceViewer";
import InterpretationViewer from "./components/InterpretationViewer/InterpretationViewer";
import ReasonerValidationOutputViewer from "./components/ReasonerValidationOutputViewer/ReasonerValidationOutputViewer";

export const AppContext = createContext();

function App() {
  const [normativeSources, setNormativeSources] = useState([]);
  const [interpretations, setInterpretations] = useState([]);
  const [reasoners, setReasoners] = useState([]);
  const [validators, setValidators] = useState([]);
  const [reasonerValidationOutputs, setReasonerValidationOutputs] = useState([]);
  const [reasonerOutputs, setReasonerOutputs] = useState([]);

  const [activeItem, setActiveItem] = useState({
    type: null,
    id: null,
  });

  const fetchReasonerValidationOutputs = async () => {
    const response = await fetch(`${API_URL}/reasoner_validation_outputs`);
    const data = await response.json();
    setReasonerValidationOutputs(data);
  };

  const fetchReasonerOutputs = async () => {
    const response = await fetch(`${API_URL}/reasoner_outputs`);
    const data = await response.json();
    setReasonerOutputs(data);
  };

  useState(() => {
    Promise.all([
      fetch(`${API_URL}/normative_sources`).then((response) => response.json()),
      fetch(`${API_URL}/interpretations`).then((response) => response.json()),
      fetch(`${API_URL}/reasoners`).then((response) => response.json()),
      fetch(`${API_URL}/validators`).then((response) => response.json()),
      fetchReasonerValidationOutputs(),
      fetchReasonerOutputs(),
    ]).then(([normativeSourcesData, interpretationsData, reasonersData, validatorsData]) => {
      setNormativeSources(normativeSourcesData);
      setInterpretations(interpretationsData);
      setReasoners(reasonersData);
      setValidators(validatorsData);
    });
  }, []);

  function ListItem({ item, activeItem, setActiveItem, type }) {
    const capitalizeFirstCharacter = (string) => string.charAt(0).toUpperCase() + string.slice(1);
    const handleClick = () => setActiveItem({ type, id: item.id });
    const isActive = activeItem.type === type && activeItem.id === item.id;
    const itemTitle = item.title || `${capitalizeFirstCharacter(type)} #${item.id}`;
    let listItemClassName = "list-group-item list-group-item-action cursor-pointer ";
    if (isActive) listItemClassName += "active";

    return (
      <li key={item.id} className={listItemClassName} onClick={handleClick}>
        {itemTitle}{" "}
        {"is_valid" in item && (
          <div className={"badge " + (item.is_valid ? "bg-success" : "bg-danger")}>
            {item.is_valid ? "Valid" : "Invalid"}
          </div>
        )}
      </li>
    );
  }

  const renderNormativeSources = () => {
    if (normativeSources.length === 0) {
      return <li className="list-group-item">No normative sources found</li>;
    }

    return normativeSources.map((normativeSource) => (
      <ListItem
        key={normativeSource.id}
        item={normativeSource}
        activeItem={activeItem}
        setActiveItem={setActiveItem}
        type="normativeSource"
      />
    ));
  };

  const renderInterpretations = () => {
    if (interpretations.length === 0) {
      return <li className="list-group-item">No interpretations found</li>;
    }

    return (
      <>
        {interpretations.length > 1 && (
          <li
            className={`list-group-item list-group-item-action cursor-pointer fw-bold ${
              activeItem.type === "interpretationDiffViewer" ? "active" : ""
            }`}
            onClick={() => setActiveItem({ type: "interpretationDiffViewer", id: null })}
          >
            Compare interpretations (DiffViewer)
          </li>
        )}
        {interpretations.map((interpretation) => (
          <ListItem
            key={interpretation.id}
            item={interpretation}
            activeItem={activeItem}
            setActiveItem={setActiveItem}
            type="interpretation"
          />
        ))}
      </>
    );
  };

  const renderReasonerValidationOutputs = () => {
    if (reasonerValidationOutputs.length === 0) {
      return <li className="list-group-item">No reasoner validation outputs found</li>;
    }

    return reasonerValidationOutputs.map((reasonerValidationOutput) => (
      <ListItem
        key={reasonerValidationOutput.id}
        item={reasonerValidationOutput}
        activeItem={activeItem}
        setActiveItem={setActiveItem}
        type="reasonerValidationOutput"
      />
    ));
  };

  const renderReasonerOutputs = () => {
    if (reasonerOutputs.length === 0) {
      return <li className="list-group-item">No reasoner outputs found</li>;
    }

    return reasonerOutputs.map((reasonerOutput) => (
      <ListItem
        key={reasonerOutput.id}
        item={reasonerOutput}
        activeItem={activeItem}
        setActiveItem={setActiveItem}
        type="reasonerOutput"
      />
    ));
  };

  return (
    <AppContext.Provider
      value={{
        normativeSources,
        interpretations,
        reasoners,
        validators,
        activeItem,
        setActiveItem,
        fetchReasonerValidationOutputs,
        fetchReasonerOutputs,
      }}
    >
      <div className="container-fluid">
        <div className="row m-2">
          <div className="col-12 text-center">
            <h1>Normative Sources and Interpretations</h1>
          </div>
          <div className="col-4">
            <h5>Normative Sources</h5>
            <ul className="list-group">{renderNormativeSources()}</ul>

            <h5>Interpretations</h5>
            <ul className="list-group">{renderInterpretations()}</ul>

            <h5>Reasoner Validation Outputs</h5>
            <ul className="list-group">{renderReasonerValidationOutputs()}</ul>

            <h5>Reasoner Outputs</h5>
            <ul className="list-group">{renderReasonerOutputs()}</ul>

            <SearchComponent
              data={[normativeSources, interpretations]}
              setActiveItem={setActiveItem}
            />
          </div>
          <div className="col-8">
            {activeItem.type === "normativeSource" && (
              <NormativeSourceViewer
                normativeSource={normativeSources.find(
                  (normativeSource) => normativeSource.id === activeItem.id
                )}
              />
            )}
            {activeItem.type === "interpretation" && (
              <InterpretationViewer
                interpretation={interpretations.find(
                  (interpretation) => interpretation.id === activeItem.id
                )}
              />
            )}
            {activeItem.type === "reasonerValidationOutput" && (
              <ReasonerValidationOutputViewer
                reasonerValidationOutput={reasonerValidationOutputs.find(
                  (reasonerValidationOutput) => reasonerValidationOutput.id === activeItem.id
                )}
              />
            )}
            {activeItem.type === "interpretationDiffViewer" && <DiffViewer />}
            {activeItem.type === "reasonerOutput" && (
              <ReasonerOutputViewer
                reasonerOutput={reasonerOutputs.find(
                  (reasonerOutput) => reasonerOutput.id === activeItem.id
                )}
              />
            )}
          </div>
        </div>
      </div>
    </AppContext.Provider>
  );
}

export default App;
