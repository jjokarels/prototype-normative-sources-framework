import { useState, useEffect } from "react";
import { QuickScore } from "quick-score";
import { createSearchIndex } from "../utils/searchIndex";

export const useSearch = (data, query) => {
  const [searchResults, setSearchResults] = useState([]);

  useEffect(() => {
    const index = createSearchIndex(data);
    const qs = new QuickScore(index);

    const results = qs.search(query);
    setSearchResults(results);
  }, [data, query]);

  return searchResults;
};
