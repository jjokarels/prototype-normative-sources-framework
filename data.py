
from models.repository import NormativeSource, NormativeSourceVersion, NormativeSourceFragment, Interpretation, Dependency
from models.reasoning import ReasonerParams
from wetten_to_model import load_normative_source_from_xml_file

normative_sources = [
    NormativeSource(id="1", title="Norms interpreted in eFLINT", versions=[
        NormativeSourceVersion(version="Basic", fragments=[
            NormativeSourceFragment(
                id="1", content="This is an example norm, not interpreted."),
            NormativeSourceFragment(
                id="2", content="This norm has multiple interpretations.")
        ]),
        NormativeSourceVersion(version="Interpretation with dependencies", fragments=[
            NormativeSourceFragment(
                id="3", content="This is an example norm whose interpretation has a dependency.")
        ]),
        NormativeSourceVersion(version="Interpretation with an invalid interpretation", fragments=[
            NormativeSourceFragment(
                id="4", content="This is an example norm whose interpretation is invalid."),
            NormativeSourceFragment(
                id="8", content="This one is valid, but it can show that a ReasonerValidationOutput error can help you find the invalid one.")
        ]),
        NormativeSourceVersion(version="Interpretation with a violation", fragments=[
            NormativeSourceFragment(
                id="5", content="This is an example norm in which you can discover a violation when reasoning and ReasonerParam #2 is used.")
        ]),
    ]),
    NormativeSource(id="2", title="Norms interpreted in Prolog", versions=[
        NormativeSourceVersion(version="Income support 1.0", fragments=[
            NormativeSourceFragment(
                id="6", content="This norm says that if a person's income is lower than 1500, that person is eligible for income support. However, some may interpret this norm as 20.")
        ]),
        NormativeSourceVersion(version="Income support 2.0", fragments=[
            NormativeSourceFragment(
                id="7", content="This norm says that if a person's income is lower than 2000, that person is eligible for income support.")
        ]),
    ]),
    NormativeSource(id="gdpr_example", title="GDPR example", versions=[
        NormativeSourceVersion(version="GDPR example", fragments=[
            NormativeSourceFragment(
                id="Article 5", content="Article 5 of the GDPR outlines the principles for processing personal data. "),
            NormativeSourceFragment(
                id="Article 6", content="Article 6 of the GDPR outlines the conditions for lawful processing of personal data. ")
        ])
    ]),
    load_normative_source_from_xml_file(),
]


gdpr_eflint_file_consent = open(
    "examples/minimal-gdpr/consent.eflint", "r").read()
gdpr_eflint_file_rectification = open(
    "examples/minimal-gdpr/rectification.eflint", "r").read()
ontology_eflint_file = open(
    "examples/minimal-gdpr/ontology.eflint", "r").read()
ontology_dependency = Dependency(
    id="1", title="ontology.eflint", content=ontology_eflint_file)

# source https://gitlab.com/eflint/eflint-examples
help_with_homework_eflint_file = """Fact person Identified by String
Placeholder parent    For person
Placeholder child     For person

Fact natural-parent   Identified by parent * child
Fact adoptive-parent  Identified by parent * child
Fact legal-parent     Identified by parent * child
  Holds when adoptive-parent(parent,child) 
          || natural-parent(parent,child)

Act ask-for-help
  Actor      child
  Recipient  parent
  Creates    help-with-homework(parent,child)
  Holds when legal-parent(parent,child)

Fact homework-due Identified by child

Duty help-with-homework
  Holder        parent
  Claimant      child
  Violated when homework-due(child)

Act help 
  Actor      parent 
  Recipient  child
  Terminates help-with-homework(parent,child)
  Holds when help-with-homework(parent,child).

"""

interpretations = [
    Interpretation(id="1", fragment_id="2",
                   content="""Fact person Identified by String
Placeholder parent    For person
Placeholder child     For person""", language="eflint"),
    Interpretation(id="2", fragment_id="2",
                   content="""Fact natural-parent   Identified by parent * child
Fact adoptive-parent  Identified by parent * child
Fact legal-parent     Identified by parent * child
""", language="eflint"),
    Interpretation(id="3", fragment_id="2",
                   content="""Fact natural-parent   Identified by parent * child
Fact adoptive-parent  Identified by parent * child
Fact legal-parent     Identified by parent * child
    Holds when adoptive-parent(parent,child)
            || natural-parent(parent,child)
""", language="eflint"),
    Interpretation(id="4", fragment_id="3",
                   content=gdpr_eflint_file_consent, language="eflint", dependencies=[ontology_dependency]),
    Interpretation(id="5", fragment_id="4",
                   content="""This text is not valid eflint code""", language="eflint"),
    Interpretation(id="6", fragment_id="5",
                   content=help_with_homework_eflint_file, language="eflint"),
    Interpretation(id="7", fragment_id="6",
                   content="eligible(X) :- income(X, Y), Y < 1500.", language="prolog"),
    Interpretation(id="8", fragment_id="6",
                   content="eligible(X) :- income(X, Y), Y < 20.", language="prolog"),
    Interpretation(id="9", fragment_id="7",
                   content=""":- consult('template.pl').

income_support_eligible(Name, Income) :- Income < 1000,write(Name), write(' is eligible for income support.'), nl.

income_support_not_eligible(Name, Income) :- Income >= 1000, write(Name), write(' is not eligible for income support.'), nl.
""", language="prolog", dependencies=[
                       Dependency(id="2", title="template.pl", content=""":- dynamic income_support/2.

income_support(Name, Income) :-
  assertz(income_support(Name, Income)).
""")]),
    Interpretation(id="10", fragment_id="8",
                   content="""Fact person Identified by String
Placeholder parent    For person
Placeholder child     For person""", language="eflint"),
    Interpretation(id="11", fragment_id="Article 5",
                   content=gdpr_eflint_file_consent, language="eflint", dependencies=[ontology_dependency]),
    Interpretation(id="12", fragment_id="Article 6",
                   content=gdpr_eflint_file_rectification, language="eflint", dependencies=[ontology_dependency]),
]

reasoner_validation_outputs = []

reasoner_params = [
    ReasonerParams(
        id="1",
        language="eflint",
        content="""Fact person Identified by Alice, Bob, Chloe, David.

+natural-parent(Alice, Bob).
+adoptive-parent(Chloe, David).

?legal-parent(Alice, Bob).
?legal-parent(Chloe, David).
?!legal-parent(Alice, Chloe).
?!legal-parent(Alice, David).

""",),
    ReasonerParams(
        id="2",
        language="eflint",
        content="""Fact person Identified by Alice, Bob, Chloe, David.

+natural-parent(Alice, Bob).
+adoptive-parent(Chloe, David).

?legal-parent(Alice, Bob).
?legal-parent(Chloe, David).
?!legal-parent(Alice, Chloe).
?!legal-parent(Alice, David).

ask-for-help(Bob, Alice).
+homework-due(Bob).  // homework deadline passed 

""",),
]

reasoner_outputs = []

# fmt: off
from validators.prolog_validator_example import PrologValidator
from validators.eflint_validator_example import EFlintValidator
from reasoners.eflint_reasoner_jonathans_eflint_gui import EFlintReasonerJonathansEFlintGUI
from reasoners.eflint_reasoner_repl_interactive import EFlintReasonerREPLInteractive
from reasoners.eflint_reasoner_repl_querying import EFlintReasonerREPLQuerying
from reasoners.prolog_reasoner_swipl import PrologReasonerSwipl
# fmt: on

reasoners = [
    PrologReasonerSwipl(),
    EFlintReasonerREPLQuerying(),
    EFlintReasonerREPLInteractive(),
    EFlintReasonerJonathansEFlintGUI()
]


validators = [
    EFlintValidator(),
    PrologValidator()
]
