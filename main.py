from typing import List, Optional
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from data import normative_sources, interpretations, reasoner_validation_outputs, reasoner_params, reasoner_outputs, reasoners, validators
from models.reasoning import ReasonerValidationOutput, ReasonerValidationError, ReasonerInput, ReasonerPackage

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5173", "http://localhost:4173",
                   "https://jjokarels.gitlab.io"],
    allow_methods=["GET", "POST"],
    allow_headers=["*"],
)


@app.get("/normative_sources")
async def get_normative_sources():
    return normative_sources


@app.get("/interpretations")
async def get_interpretations():
    return interpretations


@app.get("/reasoners/")
async def get_reasoners():
    return reasoners


@app.get("/validators/")
async def get_validators():
    return validators


@app.get("/reasoner_validation_outputs")
async def get_reasoner_validation_outputs():
    return reasoner_validation_outputs


@app.get("/reasoner_params/")
async def get_reasoner_params():
    return reasoner_params


@app.get("/reasoner_outputs/")
async def get_reasoner_outputs():
    return reasoner_outputs


@app.post("/validation_for_reasoning")
async def validation_for_reasoning(interpretation_ids: List[str], validator_index: int) -> ReasonerValidationOutput:
    selected_interpretations = [
        interpretation for interpretation in interpretations if interpretation.id in interpretation_ids]
    languages = set(
        interpretation.language for interpretation in selected_interpretations)
    language = languages.pop() if len(languages) == 1 else "undefined"
    reasoner_input = ReasonerInput(
        interpretations=selected_interpretations, language=language)

    validator = None
    if validator_index < len(validators):
        validator = validators[validator_index]
        if language != validator.language:
            validator = None
            raise HTTPException(
                status_code=404, detail="Validator language does not match")

    if validator is None:
        error_message = "Unable to join interpretations from different languages" if len(
            languages) > 1 else "Validator not found"
        return create_reasoner_validation_output(False, ReasonerValidationError(error={"message": error_message}, reasoner_input=reasoner_input))

    validate_result, joint = validator.validate(reasoner_input)

    if validate_result:
        return create_reasoner_validation_output(True, ReasonerPackage(reasoner_input=reasoner_input, joint=joint))
    else:
        trouble_shoot_dict = {f"Interpretation {interpretation.id} valid?": "Yes" if validator.validate(ReasonerInput(
            interpretations=[interpretation], language=language))[0] else "No" for interpretation in reasoner_input.interpretations}
        return create_reasoner_validation_output(False, ReasonerValidationError(error=trouble_shoot_dict, reasoner_input=reasoner_input))


def create_reasoner_validation_output(is_valid, output) -> ReasonerValidationOutput:
    reasoner_validation_output = ReasonerValidationOutput(
        id=len(reasoner_validation_outputs) + 1, is_valid=is_valid, output=output)
    reasoner_validation_outputs.append(reasoner_validation_output)
    return reasoner_validation_output


@app.post("/launch_reasoner_validation_output/{reasoner_validation_output_id}")
async def launch_reasoner_validation_output(reasoner_validation_output_id: str, reasoner_index: int, reasoner_params_id: Optional[str] = None):
    reasoner_validation_output = next(
        (output for output in reasoner_validation_outputs if output.id == reasoner_validation_output_id and output.is_valid), None)
    included_reasoner_params = next(
        (params for params in reasoner_params if params.id == reasoner_params_id), None)

    if reasoner_validation_output is None or not reasoner_validation_output.is_valid:
        raise HTTPException(
            status_code=404, detail="Reasoner package not found or not valid")

    reasoner_package = reasoner_validation_output.output
    language = reasoner_package.reasoner_input.language
    reasoner = reasoners[reasoner_index] if reasoner_index < len(
        reasoners) and reasoners[reasoner_index].language == language else None

    if reasoner is None:
        raise HTTPException(status_code=404, detail="Reasoner not found")

    return reasoner.start_reasoner(reasoner_validation_output, included_reasoner_params)
