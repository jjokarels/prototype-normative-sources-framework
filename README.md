# Framework for Storing and Reasoning About Normative Sources With Formalised Interpretations Prototype Implementation

This repository holds a prototype implementation of the framework that was designed in the Design of a Framework for Storing and Reasoning About Normative Sources With Formalised Interpretations 2023 Thesis.

## TL;DR

To get started, set up the Python FastAPI back end. Install the required dependencies, then start the back end on port 7979. Make sure to meet the prerequisites for eFLINT and Prolog if you want to use validators and reasoners. Access the front end either on [https://jjokarels.gitlab.io/prototype-normative-sources-framework](https://jjokarels.gitlab.io/prototype-normative-sources-framework) or run it locally on port 4173.

## Installation

For a working installation, you need to set up at least the back end and either have access to the [GitLab Pages deployment](https://jjokarels.gitlab.io/prototype-normative-sources-framework) of the front end or set up the front end locally. Start by cloning this repository, and navigate to the cloned repository in your terminal.

---

### Back end

The back end is written in Python and uses FastAPI.

#### Prerequisites

- Python (development was done using Python 3.10.5)
- pip

#### Installation

`pip install -r requirements.txt`

#### Starting the back end

`uvicorn main:app --port 7979` will start the back end on port 7979. The back end will be available at `http://localhost:7979`.

If you want to run the back end on a different port, change the port number in the command above. The port number 7979 was chosen to not interfere with the proxy used in Jonathan's eFLINT GUI, which is running on port 8000. Note that if you change the port, you need to change the `API_URL` variable in the frontend folder in the [API_URL.js](frontend/src/API_URL.js) file as well.

- _New data such as reasoner (validation) outputs are non-persistent in this implementation, and will be lost when the back end is restarted._
- _Adding the `--reload` flag to the start command will reload the back end when changes are made to the code. This is useful when developing._

### ⚠️ Back end: validators and reasoners with eFLINT and Prolog

#### eFLINT (TL;DR: install Haskell implementation, run EFLINT Web Server on port 8080, optionally set up Jonathan's eFLINT GUI)

Any eFLINT validation or reasoning requires the [EFLINT Server web server](https://github.com/mostafamohajeri/eflint-server) **to be running on port 8080**. This in turn requires the [eFLINT Haskell Implementation](https://gitlab.com/eflint/haskell-implementation) to be installed. The Haskell implementation also includes the eFLINT REPL, which is used by two reasoner implementations in this prototype. Note that Jonathan's eFLINT GUI has the same requirements.

If you want to reason in [Jonathan's eFLINT GUI](https://github.com/one-jonathan/jonathans-eflint-gui), you need to clone its repository, install the prerequisites (not docker), and run `npm install && npm run build` in the repository, and then put the path of the cloned repository in the `pathToJonathansEFlintGUI` variable in the [EFlintReasonerJonathansEFlintGUI](reasoners/eflint_reasoner_jonathans_eflint_gui.py) file. Note that Jonathan's eFLINT GUI Docker setup will not work as the Python back end will place folders in a file on your system that the Docker container cannot access. The [EFlintReasonerJonathansEFlintGUI](reasoners/eflint_reasoner_jonathans_eflint_gui.py) Reasoner will not automatically create an instance for you but will help you so that in the started web browser you only need to press the "Submit" button in the selector component.

**The implementation of checking eFLINT code, for example for validity or when combining specifications, is still preliminary in this implementation. If you move to an environment where you need a robust solution, this is likely to require further critical review.**

#### Prolog

Any Prolog validation or reasoning requires SWI-Prolog to be installed. You can download [here](https://www.swi-prolog.org/download/stable?show=all). Note that pyswip, the Python package used to interface with Prolog, does not seem to work properly with versions >= 9.0, so we recommend SWI-Prolog version 8.4.2.

_Sometimes Prolog validation will take over your command line. If this happens, press ENTER a few times to finish validation._

---

### Accessing the front end via GitLab Pages

Assuming you are running the back end on `localhost:7979`, you can access the front end via [https://jjokarels.gitlab.io/prototype-normative-sources-framework](https://jjokarels.gitlab.io/prototype-normative-sources-framework), which means you don't have to set up the React application yourself.

---

### Setting up the front end locally

The front end is written in JavaScript and uses React.

#### Prerequisites

- Node (development was done using v16.16.0)

#### Installation

- Change the directory to the frontend folder.
- `npm install` in the frontend folder.

#### Starting the front end

- Then run `npm run build` in the frontend folder, and run `npm run preview` to start the front end on port 4173. The front end will be available at `http://localhost:4173`.
- You can also run `npm run dev` to start the development server at port 5173.

---

_The back end will create temporary folders in the project root folders. These are used for validation and reasoning. When done, you can delete these folders._

_You can change data of normative sources and interpretations in the [data.py](data.py) file. When extending with your own reasoner or validator, you can add it to the `reasoners` or `validators` lists in this file. Make sure to restart the back end after making changes to this file._
