from validators.validator_base import ValidatorBase
from models.reasoning import ReasonerInput
from util import write_file, write_dependencies, eflint_code_combinator
from typing import Tuple
import requests
import json
import os
import time


def validate_eflint(combined_content, interpretations) -> bool:
    reasoner_folder = f"temp-validate-eflint-{time.time_ns()}"
    while os.path.exists(reasoner_folder):
        reasoner_folder = f"temp-validate-eflint-{time.time_ns()}"
    os.makedirs(reasoner_folder)

    try:
        reasoner_file = "_main.eflint"
        reasoner_file_path = os.path.join(reasoner_folder, reasoner_file)
        write_dependencies(interpretations, reasoner_folder)
        write_file(reasoner_file_path, combined_content)
        absolute_path = os.path.abspath(reasoner_file_path)

        url = "http://localhost:8080/create"

        payload = {
            "template-name": absolute_path,
            "values": {}
        }
        headers = {
            "Content-Type": "application/json"
        }

        response = requests.post(
            url, data=json.dumps(payload), headers=headers)
        response = json.loads(response.text)

        if response.get("status") != "SUCCESS":
            raise Exception("First eFLINT validation request failed")

        url = "http://localhost:8080/command"
        uuid = response.get("data").get("uuid")
        payload = {
            "uuid": uuid,
            "request-type": "command",
            "data": {
                "command": "status"
            }
        }
        new_response = requests.post(
            url, data=json.dumps(payload), headers=headers)
        new_response = json.loads(new_response.text)

        if new_response is None:
            raise Exception("No response from eFLINT server")

        payload = {
            "uuid": uuid,
            "request-type": "command",
            "data": {
                "command": "kill"
            }
        }
        killResponse = requests.post(
            url, data=json.dumps(payload), headers=headers)
        killResponse = json.loads(killResponse.text)

        return new_response.get("data").get("response").get("response") == "success"
    except Exception as e:
        print(e)
        return False


class EFlintValidator(ValidatorBase):
    def __init__(self):
        self.name = "eFLINT Validator"
        self.language = "eflint"

    def validate(self, reasoner_input: ReasonerInput) -> Tuple[bool, str]:
        combined_content = "\n".join(
            interpretation.content for interpretation in reasoner_input.interpretations
        )
        combined_content = eflint_code_combinator(combined_content)
        return validate_eflint(combined_content, reasoner_input.interpretations), combined_content
