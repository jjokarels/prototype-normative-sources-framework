from models.reasoning import ReasonerInput
from typing import Tuple


class ValidatorBase:
    def __init__(self):
        self.name = ""
        self.language = ""

    def validate(self, reasoner_input: ReasonerInput) -> Tuple[bool, str]:
        raise NotImplementedError(
            "validate method must be implemented in derived classes")
