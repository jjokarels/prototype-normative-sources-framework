from validators.validator_base import ValidatorBase
from models.reasoning import ReasonerInput
from pyswip import Prolog
from typing import Tuple

# Sometimes, the Prolog() call will take over your terminal.
# Press enter till the cmd is free again, the function will finish.

# WARNING: This is a very simple implementation and may not work for all cases.


class PrologValidator(ValidatorBase):
    def __init__(self):
        self.name = "Prolog Validator"
        self.language = "prolog"

    def validate(self, reasoner_input: ReasonerInput) -> Tuple[bool, str]:
        combined_content = "\n".join(
            interpretation.content for interpretation in reasoner_input.interpretations
        )
        return self.validate_prolog(combined_content), combined_content

    def validate_prolog(self, combined_content, dependencies=[]):
        prolog = Prolog()
        statements = [x.strip()
                      for x in combined_content.split("\n") if x.strip()]

        for statement in statements:
            try:
                print("Asserting: " + statement[:-1])
                prolog.assertz(statement[:-1])
            except Exception as e:
                # if any statement is invalid, return False
                print(str(e))
                return False

        return True
