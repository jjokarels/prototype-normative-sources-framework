import xml.etree.ElementTree as ET
from models.repository import NormativeSource, NormativeSourceVersion, NormativeSourceFragment
from time import time_ns
"""
This script contains functions to parse and load normative sources from XML files downloaded from wetten.nl, such as https://wetten.overheid.nl/BWBR0003748/1985-06-01/.
"""


def extract_text(element):
    text = ''
    if element.text:
        text += element.text.strip() + ' '
    for sub_element in element:
        text += extract_text(sub_element)
        if sub_element.tail:
            text += sub_element.tail.strip() + ' '
    return text


def parse_xml(xml_str: str) -> NormativeSource:
    root = ET.fromstring(xml_str)
    normative_source_id = root.get("bwb-id")
    title_element = root.find(".//citeertitel")
    title = title_element.text if title_element is not None else ""
    version = root.find(
        ".//ondertekeningsdatum").text if root.find(".//ondertekeningsdatum") is not None else ""

    versions = [
        NormativeSourceVersion(
            version=version,
            fragments=[
                NormativeSourceFragment(
                    id=article.get('label') if article.get(
                        'label') is not None else f"Unknown article parsed at time {time_ns()}",
                    content='\n'.join([
                        extract_text(al)
                        for al in article.findall('.//al')
                    ]).strip()
                )
                for article in root.findall(".//artikel")
            ]
        )
    ] if root.findall(".//artikel") else []

    normative_source = NormativeSource(
        id=normative_source_id,
        title=title,
        versions=versions
    )

    return normative_source


def load_normative_source_from_xml_file(filename: str = "examples\BWBR0003748_1985-06-01_0.xml") -> NormativeSource:
    with open(filename, "r") as f:
        xml_string = f.read()

    normative_source = parse_xml(xml_string)

    for version in normative_source.versions:
        print(
            f"wetten_to_model.py - Loaded {len(version.fragments)} fragments for version {version.version} of Source {normative_source.id} given by file {filename}")
    return normative_source
