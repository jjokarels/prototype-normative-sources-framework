from reasoners.reasoner_base import ReasonerBase
from models.reasoning import ReasonerValidationOutput, ReasonerOutput, ReasonerParams
from util import init_reasoner_folder, write_dependencies, write_file
from data import reasoner_outputs
from typing import Optional
import os
import subprocess
import requests
import time
import json


def checkViaServerForViolations(absolute_path):
    url = "http://localhost:8080"
    headers = {"Content-Type": "application/json"}

    payload = {
        "template-name": absolute_path,
        "values": {}
    }

    response = requests.post(
        f"{url}/create", data=json.dumps(payload), headers=headers)
    response = response.json()

    if response.get("status") != "SUCCESS":
        return None

    uuid = response.get("data").get("uuid")
    payload = {
        "uuid": uuid,
        "request-type": "command",
        "data": {
            "command": "status"
        }
    }

    response = requests.post(
        f"{url}/command", data=json.dumps(payload), headers=headers)
    status = response.json()

    if response is None:
        raise Exception("No response from eflint server")

    payload = {
        "uuid": uuid,
        "request-type": "command",
        "data": {
            "command": "kill"
        }
    }

    response = requests.post(
        f"{url}/command", data=json.dumps(payload), headers=headers)
    response = response.json()

    return status.get("data").get("response").get("violations")


def checkViolations(construct, paramsContent, interpretations) -> list:
    def generate_reasoner_folder():
        folder_name = f"temp-checkviolations-eflint-{time.time_ns()}"
        while os.path.exists(folder_name):
            folder_name = f"temp-checkviolations-eflint-{time.time_ns()}"
        os.makedirs(folder_name)
        return folder_name

    def write_interpretation_file(interpretation, folder_name):
        combined_construct = interpretation.content + \
            ("\n" + paramsContent if paramsContent is not None else "")
        reasoner_file = "_main.eflint"
        reasoner_file_path = os.path.join(folder_name, reasoner_file)
        write_file(reasoner_file_path, combined_construct)
        return os.path.abspath(reasoner_file_path)

    def check_violations(absolute_path):
        return checkViaServerForViolations(absolute_path)

    reasoner_folder = generate_reasoner_folder()

    write_dependencies(interpretations, reasoner_folder)
    main_file = os.path.join(reasoner_folder, "_main.eflint")
    write_file(main_file, construct)
    absolute_path = os.path.abspath(main_file)

    try:
        violations = check_violations(absolute_path)
        if not violations:
            return []
    except Exception as e:
        print(e)
        return []

    violationsInterpretationIds = []
    for interpretation in interpretations:
        reasoner_folder = generate_reasoner_folder()

        try:
            write_dependencies([interpretation], reasoner_folder)
            reasoner_file_path = write_interpretation_file(
                interpretation, reasoner_folder)
            absolute_path = os.path.abspath(reasoner_file_path)

            violations = check_violations(absolute_path)
            if violations:
                violationsInterpretationIds.append(interpretation.id)
        except Exception as e:
            print(e)
            return None

    return violationsInterpretationIds


class EFlintReasonerREPLQuerying(ReasonerBase):
    def __init__(self):
        super().__init__()
        self.name = "eFLINT Reasoner REPL With Querying and Validation Output (Requires Web Server)"
        self.language = "eflint"
        self.uses_params = True
        self.has_reasoner_output = True

    def start_reasoner(self, reasoner_validation_output: ReasonerValidationOutput,
                       reasoner_params: Optional[ReasonerParams] = None) -> Optional[ReasonerOutput]:
        reasoner_input = reasoner_validation_output.output.reasoner_input
        reasoner_package = reasoner_validation_output.output
        reasoner_file_path, construct = init_reasoner_folder(
            reasoner_package, reasoner_input, reasoner_params, self.language)

        command = f'eflint-repl {reasoner_file_path}'
        process = subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        timeout = 1
        time.sleep(timeout)

        violations = checkViolations(
            construct, reasoner_params.content if reasoner_params is not None else "", reasoner_input.interpretations)

        if violations:
            output = "Violations found in the following interpretations: " + \
                ", ".join(violations)
        else:
            output = "No violations found"

        if process.poll() is None:
            process.terminate()
        stdout, _ = process.communicate()
        output = output + "\n" + stdout.decode().strip()

        returnObject = ReasonerOutput(
            reasoner_validation_output_id=reasoner_validation_output.id,
            reasoner_input=reasoner_validation_output.output.reasoner_input,
            reasoner_params=reasoner_params,
            id=len(reasoner_outputs) + 1,
            output=output,
        )
        reasoner_outputs.append(returnObject)
        return returnObject
