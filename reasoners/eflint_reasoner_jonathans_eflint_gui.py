from reasoners.reasoner_base import ReasonerBase
from models.reasoning import ReasonerValidationOutput, ReasonerOutput, ReasonerParams
from util import init_reasoner_folder
from data import reasoner_outputs
from typing import Optional
import threading
import webbrowser
import socket
import os
import time

# Important! This path must be set to the path of the jonathans-eflint-gui project directory
# Next to this, you must have already executed npm install and npm run build in the jonathans-eflint-gui project directory
# You can also not run this Python application on port 8000, as this is used by the GUI.
pathToJonathansEFlintGUI = "C:/jonathans-eflint-gui-master"

# In this file, we mostly deal with launching the GUI. We assume that if ports 3000 and 8000 are occupied, the GUI is already running.
# If we would kill the process running the GUI in this program, we would disable users accessing the GUI, which is not what we want.
# Therefore users must kill the GUI themselves in the terminal of the current Python application.

# The ?filePath parameter in the link is used to help users create an instance with reasoner input. Jonathan's eFLINT GUI will not
# automatically load the reasoner input, but it will show the file path in the GUI. Users can then click the "Submit"
# button to load the reasoner input.


def port_is_occupied(port: int) -> bool:
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('localhost', port))
        return True
    except ConnectionRefusedError:
        return False
    finally:
        sock.close()


class EFlintReasonerJonathansEFlintGUI(ReasonerBase):
    def __init__(self):
        super().__init__()
        self.name = "eFLINT Reasoner Jonathan's EFlint GUI (slow set-up, requires npm installation and predefined path)"
        self.language = "eflint"
        self.uses_params = True
        self.has_reasoner_output = False

    def start_reasoner(self, reasoner_validation_output: ReasonerValidationOutput,
                       reasoner_params: Optional[ReasonerParams] = None) -> Optional[ReasonerOutput]:
        reasoner_input = reasoner_validation_output.output.reasoner_input
        reasoner_package = reasoner_validation_output.output
        reasoner_file_path, _ = init_reasoner_folder(
            reasoner_package, reasoner_input, reasoner_params, self.language)
        absolute_path = os.path.abspath(reasoner_file_path)

        current_directory = os.getcwd()
        try:
            os.chdir(pathToJonathansEFlintGUI)

            def run_npm_serve():
                os.system("npm run serve")

            def open_browser():
                webbrowser.open(
                    f'http://localhost:3000/?filePath={absolute_path}')

            if not port_is_occupied(3000) and not port_is_occupied(8000):
                thread1 = threading.Thread(target=run_npm_serve)
                thread1.start()
                time.sleep(5)

            thread2 = threading.Thread(target=open_browser)
            thread2.start()

        except Exception as e:
            print(e)
            return None
        finally:
            os.chdir(current_directory)

        output = f'Reasoner {self.name} started with {reasoner_file_path}. Close in terminal to stop.'

        returnObject = ReasonerOutput(
            reasoner_validation_output_id=reasoner_validation_output.id,
            reasoner_input=reasoner_validation_output.output.reasoner_input,
            reasoner_params=reasoner_params,
            id=len(reasoner_outputs) + 1,
            output=output,
        )
        reasoner_outputs.append(returnObject)
        return returnObject
