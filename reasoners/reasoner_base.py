from models.reasoning import ReasonerValidationOutput, ReasonerOutput, ReasonerParams
from typing import Optional


class ReasonerBase:
    def __init__(self):
        self.name = ""  # Reasoner name
        self.language = ""  # Language of files used
        self.uses_params = False  # Indicates whether sent parameters are used.
        self.has_reasoner_output = False
        # Indicates whether the output actually contains useful information about the reasoning process, 'Reasoner started' is not a useful output.

    def start_reasoner(self, reasoner_validation_output: ReasonerValidationOutput,
                       reasoner_params: Optional[ReasonerParams] = None) -> Optional[ReasonerOutput]:
        raise NotImplementedError(
            "Subclasses must implement start_reasoner method")
