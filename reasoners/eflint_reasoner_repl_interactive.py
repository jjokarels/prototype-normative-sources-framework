from reasoners.reasoner_base import ReasonerBase
from models.reasoning import ReasonerValidationOutput, ReasonerOutput, ReasonerParams
from util import init_reasoner_folder
from data import reasoner_outputs
from typing import Optional
import subprocess


class EFlintReasonerREPLInteractive(ReasonerBase):
    def __init__(self):
        super().__init__()
        self.name = "eFLINT Reasoner REPL"
        self.language = "eflint"
        self.uses_params = True
        self.has_reasoner_output = False

    def start_reasoner(self, reasoner_validation_output: ReasonerValidationOutput,
                       reasoner_params: Optional[ReasonerParams] = None) -> Optional[ReasonerOutput]:
        reasoner_input = reasoner_validation_output.output.reasoner_input
        reasoner_package = reasoner_validation_output.output
        reasoner_file_path, _ = init_reasoner_folder(
            reasoner_package, reasoner_input, reasoner_params, self.language)

        subprocess.Popen(
            ['start', 'cmd', '/K', f'eflint-repl {reasoner_file_path}'], shell=True)

        output = f'Reasoner {self.name} started at {reasoner_file_path}'

        returnObject = ReasonerOutput(
            reasoner_validation_output_id=reasoner_validation_output.id,
            reasoner_input=reasoner_validation_output.output.reasoner_input,
            reasoner_params=reasoner_params,
            id=len(reasoner_outputs) + 1,
            output=output,
        )
        reasoner_outputs.append(returnObject)
        return returnObject
