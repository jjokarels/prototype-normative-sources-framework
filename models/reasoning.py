from typing import List, Dict, Optional
from pydantic import BaseModel
from .repository import Interpretation


class ReasonerInput(BaseModel):
    interpretations: List[Interpretation]
    language: str


class ReasonerPackage(BaseModel):
    reasoner_input: ReasonerInput
    joint: str


class ReasonerValidationError(BaseModel):
    reasoner_input: ReasonerInput
    error: Dict[str, str]


class ReasonerValidationOutput(BaseModel):
    id: str
    is_valid: bool
    output: ReasonerPackage | ReasonerValidationError


class ReasonerParams(BaseModel):
    id: str
    language: str
    content: str


class ReasonerOutput(BaseModel):
    id: str
    reasoner_validation_output_id: str
    reasoner_input: ReasonerInput
    reasoner_params: Optional[ReasonerParams]
    output: str


class Reasoner(BaseModel):
    input: ReasonerPackage
    params: ReasonerParams
