from pydantic import BaseModel
from typing import List, Optional


class NormativeSourceFragment(BaseModel):
    id: str
    content: str


class NormativeSourceVersion(BaseModel):
    version: str
    fragments: List[NormativeSourceFragment]


class NormativeSource(BaseModel):
    id: str
    title: str
    versions: List[NormativeSourceVersion]


class Dependency(BaseModel):
    id: str
    title: str
    content: str


class Interpretation(BaseModel):
    id: str
    fragment_id: str
    content: str
    language: str
    dependencies: Optional[List[Dependency]]
